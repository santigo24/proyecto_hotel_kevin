const Sequelize = require("sequelize");
const huesped = require("../models").Huespedes;

module.exports = {
  List(_, res) {
    return huesped
      .findAll({})
      .then((huesped) => res.status(200).send(huesped))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return huesped
      .findAll({
        where: {
          nombres: req.params.nombres,
        },
      })
      .then((huesped) => res.status(200).send(huesped))
      .catch((error) => res.status(400).send(error));
  },

  DeleteHuespedes(req, res) {
    return huesped
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((huesped) => res.sendStatus(huesped))
      .catch((error) => res.status(400).send(error));
  },
  UpdateHuespedes(req, res) {
    return huesped
      .update(
        {
          nombres: req.body.nombres,
          apellidos: req.body.apellidos,
          telefono: req.body.telefono,
          Correo: req.body.correo,
          direccion: req.body.direccion,
          ciudad: req.body.ciudad,
          pais: req.body.nombre,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateHuespedes(req, res) {
    return huesped.create({
        nombres: req.params.nombres,
        apellidos: req.params.apellidos,
        telefono: req.params.telefono,
        correo: req.params.correo,
        direccion: req.params.direccion,
        ciudad: req.params.ciudad,
        pais: req.params.pais,
      })
      .then((huesped) => res.status(200).send(huesped))
      .catch((error) => res.status(400).send(error));
  }
}
