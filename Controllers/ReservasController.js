const Sequelize = require("sequelize");
const reserva = require("../models").Reservas;

module.exports = {
  List(_, res) {
    return reserva
      .findAll({})
      .then((reserva) => res.status(200).send(reserva))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return reserva
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((reserva) => res.status(200).send(reserva))
      .catch((error) => res.status(400).send(error));
  },

  DeleteReservas(req, res) {
    return reserva
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((reserva) => res.sendStatus(reserva))
      .catch((error) => res.status(400).send(error));
  },
  UpdateReservas(req, res) {
    return reserva
      .update(
        {
          inicio_fecha: req.body.inicio_fecha,
          fin_fecha: req.body.fin_fecha,
          habitacion: req.body.habitacion,
          huesped: req.body.huesped,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateReservas(req, res) {
    return reserva
      .create({
        inicio_fecha: req.params.inicio_fecha,
        fin_fecha: req.params.fin_fecha,
        habitacion: req.params.habitacion,
        huesped: req.params.habitacion,
      })
      .then((reserva) => res.status(200).send(reserva))
      .catch((error) => res.status(400).send(error));
  }
}
