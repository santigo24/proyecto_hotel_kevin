var express = require('express');
var router = express.Router();

const HuespedesController = require('../Controllers/HuespedesController');
const HabitacionesController = require('../Controllers/HabitacionesController');
const ReservasController = require('../Controllers/ReservasController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/huespedes/',HuespedesController.List);
router.get('/api/huespedes/nombres/:nombres',HuespedesController.ListAt);
router.delete('/api/huespedes/id/:id',HuespedesController.DeleteHuespedes);
router.patch('/api/huespedes/id/:id',HuespedesController.UpdateHuespedes);
router.post('/api/huespedes/:nombres/:apellidos/:telefono/:correo/:direccion/:ciudad/:pais',HuespedesController.CreateHuespedes);

router.get('/api/habitaciones/',HabitacionesController.List);
router.get('/api/habitaciones/id/:id',HabitacionesController.ListAt);
router.delete('/api/habitaciones/id/:id',HabitacionesController.DeleteHabitaciones);
router.patch('/api/habitaciones/id/:id',HabitacionesController.UpdateHabitaciones);
router.post('/api/habitaciones/:precio_por_noche/:piso/:max_personas/:tiene_cama_bebe/:tiene_ducha/:tiene_bano/:tiene_balcon',HabitacionesController.CreateHabitaciones);

router.get('/api/reservas/',ReservasController.List);
router.get('/api/reservas/id/:id',ReservasController.ListAt);
router.delete('/api/reservas/id/:id',ReservasController.DeleteReservas);
router.patch('/api/reservas/id/:id',ReservasController.UpdateReservas);
router.post('/api/reservas/:inicio_fecha/:fin_fecha/:habitacion/:huesped',ReservasController.CreateReservas);

module.exports = router;


